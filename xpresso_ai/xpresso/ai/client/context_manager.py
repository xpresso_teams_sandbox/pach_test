__all__ = ['UserContextManager']
__author__ = 'Gopi Krishna'

from xpresso.ai.core.commons.utils.singleton import Singleton


class UserContextManager(metaclass=Singleton):
    """
    Manages context for the ongoing user session
    """

    def __init__(self):
        """
        constructor for context manager
        """
        self.data = {}

    def load_context(self, save_data):
        """
        update the information in context_manager

        updates user information i.e. uid, primary_role and project list
        :param save_data:
        :return:
        """
        self.data = save_data

    def get_context(self, filter_key=None):
        """

        :param filter_key:
        :return:
        """
        if filter_key and filter_key in self.data:
            return self.data[filter_key]
        return self.data

    def free_context(self):
        """

        :return:
        """
        self.data = {}
